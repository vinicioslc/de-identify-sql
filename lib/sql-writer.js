const { sep } = require('path');
const { parse, stringify} = require('cassandra-map');
const { createFileWriter } = require('./tools');

class SQLWriter {
  constructor(config = {
    functionsFile: null,
    outputFile: null,
    strategyDirectory: null,
  }) {
    Object.assign(this, config);

    this.schema = {};
    this.databaseWriteQueues = { default: {} };
    this.currentTableQueue = null;
    this.writeFromBuffers = this.writeFromBuffers.bind(this);
  }

  setCurrentTableQueue() {
    const { databaseName, tableName } = this.schema;
    const database = databaseName || 'default';
    let databaseQueue = this.databaseWriteQueues[database];

    if (databaseQueue === undefined) {
      databaseQueue = {};
      this.databaseWriteQueues[database] = databaseQueue;
    }

    if (databaseQueue[tableName] === undefined) {
      let writeStream = process.stdout;

      if (this.outputFile) {
        const directory = './'; // todo: parse file for path
        const filename = this.outputFile;

        writeStream = createFileWriter({ directory, filename });
      }

      let config;
      let strategies;
      try {
        strategies = this.functionsFile ? require(this.functionsFile) : null;
      } catch (err) {
        throw `Could not find master functions file (did you provide the full path?): ${this.functionsFile}`;
      }
      try {
        let configFile = databaseName ? `${databaseName}.${tableName}` : `${tableName}`;
        config = require(`${this.strategyDirectory}${sep}${configFile}.json`);

        try {
          if (!strategies) strategies = require(`${this.strategyDirectory}${sep}${configFile}.js`);
        } catch (_err) { }
        config['strategies'] = strategies || {};

      } catch (_err) {
        config = {
          columns: []
        };
      }

      const engine = require('./strategy-engine')(config);

      databaseQueue[tableName] = {
        buffer: '',
        preText: '',
        postText: '',
        engine: engine,
        writeStream,
      };
    }

    this.currentTableQueue = databaseQueue[tableName];
  }

  setSchema(schema) {
    Object.assign(this.schema, schema);
    this.setCurrentTableQueue();
  }

  setPreText(data) {
    this.currentTableQueue.preText = data;
  }

  setValues(values) {
    values = values
        .toString()
        .replace(/(NULL|\\N)(?=(?:[^'\\]*(?:\\.|'(?:[^'\\]*\\.)*[^'\\]*'))*[^']*$)/g, 'null');

    try {
      const parsed = parse(values);

      let record = {};

      for (let i = 0; i < parsed.length; i += 1) {
        const key = this.schema.columns[i].name;
        record[key] = parsed[i];
      }

      record = this.currentTableQueue.engine.process(record);
      record = Object.values(record);
      record = stringify(record);
      record = record.substring(1);
      record = record.slice(0, -1);

      this.currentTableQueue.buffer += `(${record}),`;
    } catch (err) {
      console.error('Unable to parse: ', values);
      console.error(err);
    }
  }

  async writeFromBuffers() {
    const { databaseWriteQueues } = this;

    for (const database of Object.keys(databaseWriteQueues)) {
      const databaseQueue = databaseWriteQueues[database];

      for (const tableName of Object.keys(databaseQueue)) {
        let { buffer, preText, postText, writeStream } = databaseQueue[tableName];

        if (!buffer.length) continue;

        databaseQueue[tableName].buffer = '';
        databaseQueue[tableName].preText = '';
        databaseQueue[tableName].postText = '';

        buffer = buffer.slice(0, -1);

        await new Promise((resolve) => {
          writeStream.write(preText + buffer + ";" + postText, resolve);
        });
      }
    }
  }

  async writeTailingText(text) {
    await new Promise((resolve) => {
      this.currentTableQueue.writeStream.write(text, resolve);
    });
  }

  }

module.exports = SQLWriter;
