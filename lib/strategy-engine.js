const faker = require('faker');

module.exports = function (userConfig) {
  const config = {};
  const trackedColumns = {};
  const strategies = userConfig.strategies || {};

  userConfig.columns.forEach((columnConfig) => {

    const key = typeof columnConfig.columnNum === 'number' ? columnConfig.columnNum : columnConfig.columnKey;
    let strategy;

    // Custom user strategy from .js
    if (strategies[columnConfig.redactWith]) {
      strategy = strategies[columnConfig.redactWith];
    }

    // Faker template
    if (!strategy && columnConfig.redactWith.includes('{{')) {
      try {
        faker.fake(columnConfig.redactWith);
        strategy = () => faker.fake(columnConfig.redactWith);
      } catch (_e) { }
    }

    // Faker function
    if (!strategy && columnConfig.redactWith.includes('.')) {
      const fakerPieces = columnConfig.redactWith.split('.');
      let fakerFunction = faker;
      fakerPieces.forEach((piece) => {
        if (!fakerFunction[piece]) {
          fakerFunction = null;
          return;
        }
        fakerFunction = fakerFunction[piece];
      });

      if (fakerFunction) {
        strategy = fakerFunction;
      }
    }

    // Raw value
    if (!strategy) {
      strategy = () => columnConfig.redactWith;
    }

    config[key] = {
      strategy,
      tracked: columnConfig.tracked,
    };
  });

  for (const key of Object.keys(config)) {
    trackedColumns[key] = {};
  }

  return {
    process(data) {
      for (const [key, dataValue] of Object.entries(data)) {
        if (dataValue === '' || dataValue === null) continue;

        if (config[key]) {
          let replacement = config[key].strategy();
          replacement = (isNaN(replacement)) ? replacement : Number(replacement);

          if (config[key].tracked) {
            if (trackedColumns[key][dataValue]) {
              replacement = trackedColumns[key][dataValue];
            } else {
              trackedColumns[key][dataValue] = replacement;
            }
          }

          data[key] = replacement;
        }
      }
      return data;
    },
  };
};
