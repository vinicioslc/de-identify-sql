const faker = require('faker')

let currentId = faker.datatype.number({min:100, max:999});

module.exports = {
  genericProductId: () => {
    return `${faker.datatype.number({min:100, max:999})}`;
  },
  customGenerateId: () => {
    return ++currentId;
  },
  recentDatetime: () => {
    return faker.date.past().toISOString().slice(0, 19).replace('T', ' ');
  },
  customOrderDate: () => {
    return faker.date.past().toLocaleString();
  }
}