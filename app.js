#!/usr/bin/env node

const path = require('path');
const fs = require('fs');
const yargs = require('yargs');
const packageJson = require('./package.json');
const SQLStreamer = require('./lib/sql-streamer');

const {
    functionsFile,
    inputFile,
    outputFile,
    strategyDirectory,
} = yargs
    .version(packageJson.version)
    .help('help')
    .alias('help', 'h')
    .alias('version', 'v')
    .option('outputFile', {
        alias: 'o',
        description: 'Output to file instead of stdout',
        string: true,
    })
    .option('inputFile', {
        alias: 'i',
        description: 'Read from file instead of stdin',
        string: true,
    })
    .option('strategyDirectory', {
        alias: 's',
        description: 'Location of SQL strategy files',
        string: true,
    })
    .option('functionsFile', {
        alias: 'f',
        description: 'Location of file that contains all custom functions (optional, overwrites .js per table)',
        string: true,
    })
    .default('strategyDirectory', function strategyDirectory() {
        return path.normalize(`${__dirname}${path.sep}strategy`);
    }, '{installed-dir}/strategy}')
    .argv;

const sqlStreamer = new SQLStreamer({
    functionsFile,
    outputFile,
    strategyDirectory,
});

let input = inputFile ? fs.createReadStream(path.resolve(__dirname, inputFile)) : process.stdin;
input.pipe(sqlStreamer);
